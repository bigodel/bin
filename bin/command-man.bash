# -*- sh -*-

man_command() {
  local shim_name
  shim_name="$1"

  if [ -z "$shim_name" ]; then
    printf "usage: asdf man <command> <options>\\n"
    exit 1
  fi

  local section
  if [[ $shim_name =~ \([[:digit:]]+.*\) ]]; then
      section="$(echo "$shim_name" | sed -e 's/.*(\(.*\))/\1/')"
      shim_name="$(echo "$shim_name" | cut -d'(' -f1)"
  elif [[ $shim_name =~ \..* ]]; then
      section="$(echo "$shim_name" | cut -d'.' -f2)"
      shim_name="$(echo "$shim_name" | cut -d'.' -f1)"
  elif [[ $shim_name =~ [[:digit:]] && $# -eq 2 ]]; then
    section="$1"
    shim_name="$2"
    shift
  fi

  shift

  local -a args=( "$@" )

  handle_man_command() {
    local plugin_dir=
    local plugin_name="$1"
    local version="$2"
    local -a mandirs=()

    plugin_dir="$(asdf_data_dir)/installs/${plugin_name}/${version}"
    readarray -t mandirs < <(find "${plugin_dir}" -type d -name man -print)

    [ "${#mandirs[*]}" -gt 0 ] || exit 1

    MANPATH="$(IFS=':'; echo "${mandirs[*]}")" \
           eval man "$section" "${args[@]:-$shim_name}"
  }

  with_shim_executable "$shim_name" handle_man_command || exit 1
}

man_command "$@"
