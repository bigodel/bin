#!/bin/sh
#
# wrapper for media controls using either mpv + mpvc, and mpd + mpc.
#
b=$(tput bold)
n=$(tput sgr0)

usage() {
    cat << EOF
media is a cli media interface for mpd, mpv. It basically wraps around mpc, for
mpd, mpvc for mpv, and the vol script (found in https://github.com/jpprime/bin)
allowing you to use mnemonic commands instead of remembering their own commands.

usage:
  media [-cnpfBgabtxudmM]

options:
  ${b}-h$n  Show this help
  ${b}-c$n  Get the current song
  ${b}-s$n  Show current MPD status
  ${b}-n$n  Change to next song
  ${b}-p$n  Change to previous song
  ${b}-f$n  Jump ahead 30 seconds
  ${b}-B$n  Rewind 30 seconds
  ${b}-g$n  Go to the beginning of the song
  ${b}-a$n  Play from previous point
  ${b}-b$n  Pause playback
  ${b}-t$n  Play if paused, pause if playing
  ${b}-x$n  Kill playback
  ${b}-u$n  Increase the volume
  ${b}-d$n  Decrease the volume
  ${b}-V$n  Show the volume
  ${b}-m$n  Toggle mute
  ${b}-M$n  Mute for real

If no options were passed, show the current song (same as ${b}-c${n}).

The short version of commands can also be used in successsion, like

    media -cnb - shows the current song, changes to the next song and pauses.

The commands for volume use the vol script. vol -h shows usage.
EOF
}

[ $# -eq 0 ] && set -- -c

alias MPV='[ "$(pgrep mpv)" ] && mpvc'
alias MPD='[ "$(pgrep mpd)" -o "$(pgrep mopidy)" ] && mpc -q'
alias MPDv='[ "$(pgrep mpd)" -o "$(pgrep mopidy)" ] && mpc'

playing() {
    song="$( (MPV -f '%artist% - %title%') || (MPD current) )"

    [ -z "$song" ] && exit 1

    # truncate up to 40 characters
    limit=40
    if [ ${#song} -le $limit ]; then echo "$song"
    else
        song=$(echo "$song" | cut -c -$limit | awk '{$1=$1};1')
        echo "${song}…"
    fi

}

status() {
    ( (MPV status) || (MPDv status) ) | grep -E 'paused|playing' | \
        sed -e 's/\[//g' | cut -d] -f-1
}

next() { (MPV --track +1) || (MPD next) ; }
prev() { (MPV --track -1) || (MPD prev) ; }
forward()  { (MPV --seek +10) || (MPD seek +00:00:10) ; }
back() { (MPV --seek -10) || (MPD seek -00:00:10) ; }
beginning() { (MPV --time 0) || (MPD seek 0%) ; }

pause_play() {
    case "$1" in
        play)   (MPV play  ) || (MPD play  ) ;;
        pause)  (MPV pause ) || (MPD pause ) ;;
        toggle) (MPV toggle) || (MPD toggle) ;;
        stop)   (MPV --kill) || (MPD stop  ) ;;
    esac
}

while getopts 'scnpfBgtabxmudVM?h' opt
do
    case $opt in
        c) playing ;;
        s) status ;;
        n) next ;;
        p) prev ;;
        f) forward ;;
        B) back ;;
        g) beginning ;;
        t) pause_play toggle ;;
        a) pause_play play ;;
        b) pause_play pause ;;
        x) pause_play stop ;;
        u) vol -qi ;;
        d) vol -qd ;;
        V) vol -qs ;;
        m) vol -qm ;;
        M) vol -qM ;;
        h) usage ;;
        ?) >&2 usage && exit 2 ;;
    esac
done
