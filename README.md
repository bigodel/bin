# Scripts #

I created this repository to store the scripts I've been creating to facilitate
my life on Unix systems. I thrive to have all of my scripts to be POSIX
compliant.
